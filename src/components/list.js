import { useEffect, useState } from "react";
import { getList } from "../utilitles/server";
import Conditional from "./conditional";
import Loader from "./loader";
import ListItem from "./listItem";
export default function List(){
    const [loading, setLoading]  = useState(false);
    const [recentList, setList]  = useState(null);
    useEffect(()=>{
        setLoading(true);
        getList().then(l=>{
            setLoading(false);
            if(l?.data?.length);
            setList(l);
        });
    },[]);
    return (<div className="recent-list inner-container">
        <div className="list-header">
            Most visited Links
        </div>
        <Conditional condition={!recentList?.data?.length}>
        <div className="intermediate-state">
            <Loader loading={loading}/>
            <Conditional condition={!loading}>
                <div className="blank-state">
                    Nothing here yet ¯\_(ツ)_/¯
                </div>
            </Conditional>
        </div>
        </Conditional>
        <Conditional cond={recentList}>
            <div className="list-holder">
                {recentList?.data?.length?recentList?.data.map(item=><ListItem key={item.alias} item={item} base={recentList.base}/>):null}
            </div>
        </Conditional>
    </div>);
}