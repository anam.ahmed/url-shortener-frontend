export default function ListItem(props){
    let _url = `${props.base}/${props.item.alias}`;
    return (<div className="list-item">
        <a target="_blank" rel="noreferrer" href={_url}>{_url}</a>
        <div className="visitCount">{props.item.total_visit}</div>
    </div>);
}