import { useCallback, useState } from "react";
import Loader from "./loader";
import Swal from "sweetalert2";
import Conditional from "./conditional";
import { shortenNew } from "../utilitles/server";
import copy from "copy-to-clipboard";
export default function UrlInput(props){
    const [url, setURL]         = useState("");
    const [loading, setLoading] = useState(false);
    const [recent, setRecent]   = useState(null);
    const onChangeInput = useCallback((e)=>{
        let _val = e?.target?.value;
        setURL(_val.trim());
    },[]);
    const onShortenAction = useCallback(async (e)=>{
        if(!/^https?:\/\/\S+$/.test(url)) return Swal.fire("URL is not valid!", "Please enter a valid URL to shorten", "error");
        setLoading(true);
        let shortenedResult = await shortenNew(url);
        setLoading(false);
        if(shortenedResult?.alias){
            setURL("");
            setRecent(shortenedResult);
        }
        else Swal.fire("There was a problem with your request", "Your request could not be processed", "error");
    },[url]);
    const onCopyButton = useCallback(()=>{
        if(!recent) return false;
        copy(`${recent.base}/${recent.alias}`);
    },[recent]);
    const onBack = useCallback(()=>{
        setRecent(null);
    },[]);
    return (
        <div className="url-input-holder inner-container">
            <Loader loading={loading}/>
            <Conditional condition={recent && !loading}>
                <div className="recent-holder">
                    <div className="url-header">
                        {recent?.url}
                    </div>
                    <div className="recent-url">
                        <a target="_blank" rel="noreferrer" href={`${recent?.base}/${recent?.alias}`}>{recent?.base}/{recent?.alias}</a>
                        <div className="button" onClick={onCopyButton}>Copy</div>
                    </div>
                    <div onClick={onBack} className="button back-button">← Back</div>
                </div>
            </Conditional>
            <Conditional condition={!loading && !recent}>
                <input type="text"
                onChange={onChangeInput}
                value={url}
                className="url-input-box" 
                placeholder="Enter URL here"/>
                <div className="button url-accept" onClick={onShortenAction}>Shorten</div>
            </Conditional>
        </div>
    )
}