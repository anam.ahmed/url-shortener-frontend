import {Link} from "react-router-dom";
export default function Header({to,caption}){
    return (<div className="top-button-div">
    <Link className="button btn-listmode" to={to}>{caption}</Link>
  </div>);
}