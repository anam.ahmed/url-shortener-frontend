export default function Conditional({condition = true, children, cond = true, ...props}){
    if(!condition || !cond) return null;
    return <>{children}</>
}