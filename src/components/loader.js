export default function Loader({loading = true}){
    if(!loading) return null;
    return (<div className="loader"></div>);
}