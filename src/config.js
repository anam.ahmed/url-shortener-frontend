let BASE_URL = "http://localhost:3030";

if(process.env.NODE_ENV === "production"){
    BASE_URL = "https://urlsx.herokuapp.com"
}

const exports =  {
    BASE_URL
}

export default exports;