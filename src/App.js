import logo from "./assets/logo.svg";
import List from "./components/list";
import UrlInput from "./components/urlInput";
import Header from "./components/header";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
function App() {
  return (
    <div className="container">
      <Router>
        <Switch>
          <Route exact path="/">
            <Header to="/list" caption="Most visited"/>
            <div className="input-carrier">
              <img alt="logo" className="logo" src={logo}/>
              <UrlInput/>
            </div>
          </Route>
          <Route path="/list">
            <Header to="/" caption="← Add new"/>
            <List/>
          </Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
