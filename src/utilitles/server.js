import axios from "axios";
import config from "../config";
const _sv = axios.create({baseURL: config.BASE_URL, timeout: 15000});

function shortenNew(url){
    return new Promise((resolve)=>{
        _sv.post("/add",{url}).then(data=>resolve(data.data)).catch(e=>resolve(null));
    });  
}

function getList(){
    return new Promise(resolve=>{
        _sv.get("/list").then(data=>resolve(data.data)).catch(e=>resolve([]));
    });
}

export {
    _sv as default,
    shortenNew,
    getList
}