## URL shortener frontend

Frontend project for URL shortener. Tech Stack: React, React Router

### Running and building

you need to have [Node.js](https://nodejs.org/en/) (v15 or later) installed.

1. navigate to the project folder
2. Run `npm install` or `yarn` to install dependencies.
3. Open `src/config.js` and set the right `BASE_URL` (the URL to the server application).
4. Run `npm start` or `yarn start` to start the development server. Should automatically open the browser.
5. If you want to build instead, run `npm run build` or `yarn build`. Once the build is finished, you will find the files in the `build` folder.

Thanks for reading.



